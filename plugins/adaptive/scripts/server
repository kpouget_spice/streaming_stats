#! /bin/bash

THIS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

source $THIS_DIR/../../../cfg/adaptive/config.sh

host_do() {
    ssh $SERVER DISPLAY=:0 "$@"
}

do_set_gpu_freq() {
    min=$1
    max=$2

    old_min=$(host_do cat $HOST_INTEL_GPU_FREQ_DIR/gt_min_freq_mhz)
    old_max=$(host_do cat $HOST_INTEL_GPU_FREQ_DIR/gt_max_freq_mhz)

    echo $min | host_do sudo tee $HOST_INTEL_GPU_FREQ_DIR/gt_min_freq_mhz > /dev/null
    echo $max | host_do sudo tee $HOST_INTEL_GPU_FREQ_DIR/gt_max_freq_mhz > /dev/null

    echo "GPU Frequency set to $min..$max (was $old_min..$old_max)"
}

do_perf() {
    HOST_INTEL_GPU_MIN_FREQ_FILE="$HOST_INTEL_GPU_FREQ_DIR/gt_min_freq_mhz"

    action=$1
    param=$2

    if [ -z "$action" ]; then
        echo "Please specify an action (on/off)"
        exit 1
    fi

    echo "BEF: cpu=$(host_do cat $HOST_CPU_GOV_FILES | uniq) ; \
gpu=$(host_do cat $HOST_INTEL_GPU_MIN_FREQ_FILE)"

    if [ $action == off ]; then
        host_do sudo cpupower -c all frequency-set --governor powersave \
                | grep -v "Setting cpu"
        [ -z "$param" ] && param=$HOST_INTEL_GPU_DEFAULT_PERF_OFF

        echo $param | host_do sudo tee $HOST_INTEL_GPU_MIN_FREQ_FILE > /dev/null

        host_do xset s on +dpms
    else
        if [ $action == on ] || [ $action == cpu ]; then
            host_do sudo cpupower -c all frequency-set --governor performance \
                | grep -v "Setting cpu"
        fi
        if [ $action == on ] || [ $action == gpu ]; then
            [ -z "$param" ] && param=$HOST_INTEL_GPU_DEFAULT_PERF_ON
            echo $param | host_do sudo tee $HOST_INTEL_GPU_MIN_FREQ_FILE > /dev/null
        fi
        host_do xset s off -dpms
    fi
    echo "AFT: cpu=$(host_do cat $HOST_CPU_GOV_FILES | uniq) ; \
gpu=$(host_do cat $HOST_INTEL_GPU_MIN_FREQ_FILE)"
}

ACTION="$1"
shift

if [ -z $ACTION ];
then
   echo "Please provide a subcommand ..."
   exit 1
fi

case $ACTION in
    perf)
        do_perf $*
        ;;
    gpu_freq)
        do_set_gpu_freq "$1" "$2"
        ;;
    *)
        echo "ERROR: unknown command '$ACTION' ..."
        ;;
esac
